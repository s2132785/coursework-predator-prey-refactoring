import pytest

import predator_prey


class TestAnimal:
    def test_non_zero_birth_rate(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.Animal(
                birth_rate=-0.8,
                death_rate=0.5,
                diffusion_rate=0.2,
                random_seed=1,
                name='generic_animal')

        assert str(err.value) == '`birth_rate` cannot be negative'

    def test_non_zero_death_rate(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.Animal(
                birth_rate=0.8,
                death_rate=-0.5,
                diffusion_rate=0.2,
                random_seed=1,
                name='generic_animal')

        assert str(err.value) == '`death_rate` cannot be negative'
