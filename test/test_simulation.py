import os
import random

import numpy as np
import pytest

import predator_prey


@pytest.fixture
def hare():
    hare = predator_prey.Animal(
        birth_rate=0.08,
        death_rate=0.04,
        diffusion_rate=0.2,
        random_seed=1,
        name='hare')
    yield hare

@pytest.fixture
def puma():
    puma = predator_prey.Animal(
        birth_rate=0.02,
        death_rate=0.06,
        diffusion_rate=0.2,
        random_seed=1,
        name='puma')
    yield puma

@pytest.fixture
def simulation(hare, puma, tmpdir):
    simulation = predator_prey.Simulation(
        puma=puma,
        hare=hare,
        landscape_file='map.dat',
        duration=500,
        delta_t=0.4,
        logging_interval=10,
        output_directory=tmpdir)
    yield simulation


class TestSimulation:
    def test_outputs(self, simulation):
        simulation.run()

        golden_csv = 'test/resources/averages.csv'
        generated_csv = os.path.join(simulation.output_directory, 'averages.csv')

        golden_outputs = []
        with open(golden_csv, 'r') as f:
            for line in f.readlines():
                golden_outputs.append(line.strip().split(','))

        assert os.path.exists(generated_csv)
        generated_output = []
        with open(generated_csv, 'r') as f:
            for line in f.readlines():
                generated_output.append(line.strip().split(','))

        # make sure headers are exactly the same
        assert golden_outputs[0] == golden_outputs[0]

        # make sure floating point data is within acceptable tolerance
        for golden_line, generated_line in zip(golden_outputs[1:], generated_output[1:]):
            for x, y in zip(golden_line, generated_line):
                x, y = float(x), float(y)
                assert np.allclose(x, y)

        for time_step in range(0, simulation._total_time_steps):
            if not time_step % simulation._logging_interval:
                map_name = os.path.join(simulation.output_directory,
                                        'map_{:04d}.ppm'.format(time_step))

                # make sure map exists
                assert os.path.exists(map_name)

    def test_halo_padding(self, simulation):
        landscape = simulation.landscape

        padded_height = len(landscape)
        padded_width = len(landscape[0])

        # make sure halo has the right shape
        assert padded_height == simulation._height + 2
        assert padded_width == simulation._width + 2

        for row in range(0, padded_height):
            for col in range(0, padded_width):

                # make sure that the borders are filled with 0s representing the halo
                if (row == 0 or row == padded_height - 1 or col == 0
                        or col == padded_width - 1):
                    assert landscape[row, col] == 0

    def test_popoulation_initialization(self, simulation):
        landscape = simulation.landscape
        padded_height = len(landscape)
        padded_width = len(landscape[0])

        for _, population_map in simulation.population_maps.items():

            # make sure that population_map is initialized
            assert population_map is not None

            # make sure there are no animals in the halo and water regions
            for row in range(0, padded_height):
                for col in range(0, padded_width):
                    if not landscape[row, col]:
                        assert population_map[row, col] == 0
                    else:
                        assert (
                            population_map[row, col] >= simulation._MIN_INITIAL_POPULATION and
                                population_map[row, col] <= simulation._MAX_INITIAL_POPULATION)

    def test_popoulation_initialization_with_zero_as_seed(self, simulation):
        landscape = simulation.landscape
        padded_height = len(landscape)
        padded_width = len(landscape[0])

        # reset random seeds of puma and hare to 0
        for animal_name, animal in simulation.animals.items():
            simulation._animals[animal_name]._random_seed = 0

        # reinitialize simulation
        simulation.initialize()

        for _, population_map in simulation.population_maps.items():
            # make sure there are no animals when seed is set to 0
            for row in range(0, padded_height):
                for col in range(0, padded_width):
                    assert population_map[row, col] == 0

    def test_output_directory(self, simulation):
        assert os.path.exists(simulation.output_directory)

    def test_land_neighbours_all_zeros_landscape(self, simulation, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['2 3\n', '0 0\n', '0 0\n', '0 0']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # make sure that all regions have 0 neighbours
        for row in range(len(simulation.landscape)):
            for col in range(len(simulation.landscape[0])):
                assert simulation._land_neighbours[row, col] == 0


    def test_land_neighbours(self, simulation, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 3\n', '0 1 0\n', '1 0 1\n', '0 1 0']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # land neighbours  should be equal to (1 + 1 + 1 + 1) = 4
        assert simulation._land_neighbours[2, 2] == 4

    def test_average_population_all_zeros_landscape(self, simulation, tmpdir):
        test_map =  os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['2 3\n', '0 0\n', '0 0\n', '0 0']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line )

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # make sure that there are no animals in this landscape
        assert simulation._get_average_population('hare') == 0
        assert simulation._get_average_population('puma') == 0

    def test_average_population(self, monkeypatch, simulation, tmpdir):
        def always_two(*args, **kwargs):
            return 2.0

        # monkeypatch `random.uniform` to always return 2.0
        monkeypatch.setattr(random, 'uniform', always_two)

        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['2 3\n', '0 1\n', '0 1\n', '0 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # make sure the average population is (2 + 2 + 2) / 3 = 2
        # since there are only 3 possible regions that can have animals
        assert simulation._get_average_population('hare') ==  2
        assert simulation._get_average_population('puma') ==  2

    def test_csv_header(self, simulation):
        simulation._log_average_populations(time_step=0, time=0)

        csv_path = os.path.join(simulation.output_directory, 'averages.csv')
        with open(csv_path, 'r') as f:
            header = f.readline()

        assert header == simulation._AVERAGES_CSV_HEADER

    def test_average_population_logging(self, simulation):
        simulation._log_average_populations(time_step=0, time=0)

        csv_path = os.path.join(simulation.output_directory, 'averages.csv')
        with open(csv_path, 'r') as f:
            _, log = [x.strip() for x in f.readlines()]
        time_step, time, average_num_hares, average_num_pumas = log.split(',')

        assert int(time_step) == 0
        assert int(time) == 0
        assert float(average_num_hares) == simulation._get_average_population('hare')
        assert float(average_num_pumas) == simulation._get_average_population('puma')

    def test_logging(self, simulation):
        simulation.log(time_step=0, time=0)
        csv_path = os.path.join(simulation.output_directory, 'averages.csv')
        map_path = os.path.join(simulation.output_directory,
                                'map_{:04d}.ppm'.format(0))

        assert os.path.exists(csv_path)
        assert os.path.exists(map_path)

    def test_update_step_all_zeros_landscape(self, simulation, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 3\n', '0 0 0\n', '0 0 0\n', '0 0 0']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()
        simulation.update()

        landscape = simulation.landscape
        padded_height = len(landscape)
        padded_width = len(landscape[0])

        # make sure there is no change in population maps
        for _, population_map in simulation.population_maps.items():
            for row in range(0, padded_height):
                for col in range(0, padded_width):
                    assert population_map[row, col] == 0


    def test_update_step(self, monkeypatch, simulation, tmpdir):
        def always_twenty_five(*args, **kwargs):
            return 25.0

        # monkeypatch `random.uniform` to always return 25.0
        monkeypatch.setattr(random, 'uniform', always_twenty_five)

        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 3\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # compare populations after running update step, these should
        # match manually computed values
        simulation.update()
        for row in range(1, simulation._height + 1):
            for col in range(1, simulation._width + 1):
                assert np.allclose(simulation.population_maps['hare'][row, col], 15.8000)
                assert np.allclose(simulation.population_maps['puma'][row, col], 29.4000)

        simulation.update()
        for row in range(1, simulation._height + 1):
            for col in range(1, simulation._width + 1):
                assert np.allclose(simulation.population_maps['hare'][row, col], 8.8732)
                assert np.allclose(simulation.population_maps['puma'][row, col], 32.4105)

    def test_update_step_non_negative_population(self, monkeypatch, simulation, tmpdir):
        def always_one(*args, **kwargs):
            return 1.0

        # monkeypatch `random.uniform` to always return 1.0
        monkeypatch.setattr(random, 'uniform', always_one)

        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 3\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        # reinitialize simulation with new landscape
        simulation._landscape_file = test_map
        simulation.initialize()

        # make sure population never go below 0
        for _ in range(1000):
            simulation.update()
            assert np.min(simulation.population_maps['hare']) >= 0.0
            assert np.min(simulation.population_maps['puma']) >= 0.0

    def test_empty_landscape_file(self, hare, puma, tmpdir):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.Simulation(
                puma=puma,
                hare=hare,
                landscape_file='',
                duration=500,
                delta_t=0.4,
                logging_interval=10,
                output_directory=tmpdir)
        assert (str(err.value) ==
                '`landscape_file` needs to be provided to run the simulation')

    def test_mismatch_landscape_file_height(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 3\n', '1 1 1\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(AssertionError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) ==
                'Mismatch found between listed height and landscape data')


    def test_mismatch_landscape_file_width(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['4 3\n', '1 1 1\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(AssertionError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) ==
                'Mismatch found between listed width and landscape data')

    def test_negative_height(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 -3\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(ValueError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) == '`height` should be greater than zero')

    def test_negative_width(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['-3 3\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(ValueError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) == '`width` should be greater than zero')


    def test_zero_height(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['3 0\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(ValueError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) == '`height` should be greater than zero')

    def test_zero_width(self, hare, puma, tmpdir):
        test_map = os.path.join(tmpdir, 'test_map.dat')

        landscape_data = ['0 3\n', '1 1 1\n', '1 1 1\n', '1 1 1']
        with open(test_map, 'w') as f:
            for line in landscape_data:
                f.write(line)

        with pytest.raises(ValueError) as err:
            _ = predator_prey.Simulation(puma=puma,
                                         hare=hare,
                                         landscape_file=test_map,
                                         duration=500,
                                         delta_t=0.4,
                                         logging_interval=10,
                                         output_directory=tmpdir)
        assert (str(err.value) == '`width` should be greater than zero')
