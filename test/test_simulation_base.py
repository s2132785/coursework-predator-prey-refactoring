import pytest

import predator_prey


class TestSimulationBase:
    def test_empty_output_directory_argument(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(
                output_directory='',
                duration=100,
                delta_t=0.1,
                logging_interval=5)

        assert str(err.value) == '`output_directory` needs to be provided'

    def test_none_output_directory_argument(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory=None,
                                             duration=100,
                                             delta_t=0.1,
                                             logging_interval=5)

        assert str(err.value) == '`output_directory` needs to be provided'


    def test_negative_duration(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=-10,
                                             delta_t=0.1,
                                             logging_interval=5)

        assert str(err.value) == '`duration` should be greater than zero'

    def test_negative_delta_t(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=10,
                                             delta_t=-0.1,
                                             logging_interval=5)

        assert str(err.value) == '`delta_t` should be greater than zero'

    def test_negative_logging_interval(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=10,
                                             delta_t=0.1,
                                             logging_interval=-5)

        assert str(err.value) == '`logging_interval` should be greater than zero'


    def test_zero_duration(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=0,
                                             delta_t=0.1,
                                             logging_interval=5)

        assert str(err.value) == '`duration` should be greater than zero'

    def test_zero_delta_t(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=10,
                                             delta_t=-0,
                                             logging_interval=5)

        assert str(err.value) == '`delta_t` should be greater than zero'

    def test_zero_logging_interval(self):
        with pytest.raises(ValueError) as err:
            _ = predator_prey.SimulationBase(output_directory='outputs',
                                             duration=10,
                                             delta_t=0.1,
                                             logging_interval=0)

        assert (
            str(err.value) == '`logging_interval` should be greater than zero')
