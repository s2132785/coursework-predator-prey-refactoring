class Animal:
    """Container for all animals, holds useful properties """

    def __init__(self, birth_rate, death_rate, diffusion_rate, random_seed, name):
        if birth_rate < 0:
            raise ValueError('`birth_rate` cannot be negative')

        if death_rate < 0:
            raise ValueError('`death_rate` cannot be negative')

        self._birth_rate = birth_rate
        self._death_rate = death_rate
        self._diffusion_rate = diffusion_rate
        self._name = name
        self._random_seed = random_seed

    @property
    def birth_rate(self):
        return self._birth_rate

    @property
    def death_rate(self):
        return self._death_rate

    @property
    def diffusion_rate(self):
        return self._diffusion_rate

    @property
    def name(self):
        return self._name

    @property
    def random_seed(self):
        return self._random_seed
