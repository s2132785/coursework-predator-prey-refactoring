def concatenate_lists(lists):
    """Function to concatenate lists"""

    result = []
    for l in lists:
        result.extend(l)
    return result


def swap_objects(object_a, object_b):
    """Function to swap two objects"""

    temp = object_a
    object_a = object_b
    object_b = temp
    return object_a, object_b
