import os
import random

import numpy as np
from predator_prey import utils


class SimulationBase:
    """Base class for simulations"""

    def __init__(self, output_directory, duration, delta_t, logging_interval):
        if output_directory is None or output_directory == '':
            raise ValueError('`output_directory` needs to be provided')

        if duration <= 0:
            raise ValueError('`duration` should be greater than zero')

        if delta_t <= 0:
            raise ValueError('`delta_t` should be greater than zero')

        if logging_interval <= 0:
            raise ValueError('`logging_interval` should be greater than zero')

        self._output_directory = output_directory
        self._duration = duration
        self._delta_t = delta_t
        self._logging_interval = logging_interval

    def _create_output_directory(self):
        """Helper function to create output directory"""

        if not os.path.exists(self._output_directory):
            print('Creating output directory at: {}'.format(
                self._output_directory))
            os.mkdir(self._output_directory)
        else:
            print('Output directory: {} already exists'.format(
                self._output_directory))

    def initialize(self):
        """initialize simulation state"""

        raise NotImplementedError(
            'Sublclasses should overide `initialize` method')

    def log(self):
        """logs intermediate states or other useful information"""

        raise NotImplementedError('Sublclasses should overide `log` method')

    def update(self):
        """update state of simulation, incremetatlly"""

        raise NotImplementedError('Sublclasses should overide `update` method')

    def run(self):
        """runs the simulation, should call `log` and `update` methods"""

        raise NotImplementedError('Sublclasses should overide `run` method')

    @property
    def output_directory(self):
        return self._output_directory


class Simulation(SimulationBase):
    _AVERAGES_CSV_HEADER = "Timestep,Time,Hares,Pumas\n"

    _MIN_INITIAL_POPULATION = 0
    _MAX_INITIAL_POPULATION = 5

    def __init__(self, puma, hare, landscape_file, duration, delta_t,
                 logging_interval, output_directory):

        if landscape_file is None or landscape_file == '':
            raise ValueError('`landscape_file` needs to be provided to run the simulation')

        super(Simulation, self).__init__(
            output_directory=output_directory,
            duration=duration,
            delta_t=delta_t,
            logging_interval=logging_interval)

        self._animals = {'puma': puma, 'hare': hare}
        self._population_maps = {'puma': None, 'hare': None}
        self._landscape_file = landscape_file

        self._total_time_steps = int(duration / delta_t)
        self.initialize()

    def initialize(self):
        self._initialize_landscape()
        self._initialize_populations()
        self._create_output_directory()

    def _initialize_landscape(self):
        """Reads landscape description from a file, and initializes
          related states"""

        halo = [0]
        with open(self._landscape_file, 'r') as f:
            width, height = [int(i) for i in f.readline().split(' ')]

            if width <= 0:
                raise ValueError('`width` should be greater than zero')

            if height <= 0:
                raise ValueError('`height` should be greater than zero')

            print('Width: {} Height: {}'.format(width, height))

            padded_width = width + 2  # Width including halo
            padded_height = height + 2  # Height including halo
            landscape = np.zeros((padded_height, padded_width), int)

            for row, line in enumerate(f.readlines(), start=1):
                if row > height:
                    raise AssertionError(
                        'Mismatch found between listed height and landscape data')

                # Read landscape into array
                values = line.split(' ')

                landscape_data = [int(i) for i in values]
                if not len(landscape_data) == width:
                    raise AssertionError(
                        'Mismatch found between listed width and landscape data')

                # padding with halo values.
                landscape[row] = utils.concatenate_lists(
                    [halo, landscape_data, halo])

        num_lands = np.count_nonzero(landscape)
        print("Number of land-only squares: {}".format(num_lands))

        # Pre-calculate number of land neighbours of each land square.
        land_neighbours = np.zeros((padded_height, padded_width), int)
        for row in range(1, height + 1):
            for col in range(1, width + 1):
                land_neighbours[row, col] = landscape[row - 1, col] \
                    + landscape[row + 1, col] \
                    + landscape[row, col - 1] \
                    + landscape[row, col + 1]

        self._landscape = landscape
        self._num_lands = num_lands
        self._land_neighbours = land_neighbours
        self._height = height
        self._width = width

    def _initialize_populations(self):
        """Initializes populations for all animals"""

        for animal_name, animal in self._animals.items():
            random.seed(animal.random_seed)
            population_map = self._landscape.astype(np.float32).copy()
            for row in range(1, self._height + 1):
                for col in range(1, self._width + 1):
                    if animal.random_seed == 0:
                        population_map[row, col] = 0
                    else:
                        if self._landscape[row, col]:
                            population_map[row, col] = random.uniform(
                                Simulation._MIN_INITIAL_POPULATION,
                                Simulation._MAX_INITIAL_POPULATION)
                        else:
                            population_map[row, col] = 0
            self._population_maps[animal_name] = population_map

    def _get_average_population(self, animal_name):
        """Computes average population of a animal type"""

        population_map = self._population_maps[animal_name]
        if not self._num_lands == 0:
            return np.sum(population_map) / self._num_lands
        return 0

    def _log_average_populations(self, time_step, time):
        """Hepler function to log average populations to a file"""

        csv_path = os.path.join(self._output_directory, "averages.csv")
        if time == 0:
            with open(csv_path, "w") as f:
                f.write(Simulation._AVERAGES_CSV_HEADER)

        average_num_hares = self._get_average_population('hare')
        average_num_pumas = self._get_average_population('puma')
        print("Averages. Timestep: {} Time (s): {} Hares: {} Pumas: {}".format(
            time_step, time, average_num_hares, average_num_pumas))

        with open(csv_path, "a") as f:
            f.write("{},{},{},{}\n".format(time_step, time, average_num_hares,
                                           average_num_pumas))

    def _write_maps(self, time_step):
        """Helper function to write intermediate population map to files"""

        output_file_name = os.path.join(
            self._output_directory, "map_{:04d}.ppm".format(time_step))

        hare_map = self._population_maps['hare']
        puma_map = self._population_maps['puma']
        hare_map_image = np.zeros((self._height, self._width), int)
        puma_map_image = np.zeros((self._height, self._width), int)

        max_hare_population = np.max(hare_map)
        max_puma_population = np.max(puma_map)
        for row in range(1, self._height + 1):
            for col in range(1, self._width + 1):
                if self._landscape[row, col]:
                    if max_hare_population != 0:
                        hare_pixel = (hare_map[row, col] /
                                max_hare_population) * 255
                    else:
                        hare_pixel = 0
                    if max_puma_population != 0:
                        puma_pixel = (puma_map[row, col] /
                                max_puma_population) * 255
                    else:
                        puma_pixel = 0
                    hare_map_image[row - 1, col - 1] = hare_pixel
                    puma_map_image[row - 1, col - 1] = puma_pixel
        with open(output_file_name, "w") as f:
            header = "P3\n{} {}\n{}\n".format(self._width,
                                                self._height, 255)
            f.write(header)
            for row in range(0, self._height):
                for col in range(0, self._width):
                    if self._landscape[row + 1, col + 1]:
                        f.write("{} {} {}\n".format(
                            hare_map_image[row, col], puma_map_image[row, col], 0))
                    else:
                        f.write("{} {} {}\n".format(0, 0, 255))


    def log(self, time_step, time):
        self._log_average_populations(time_step=time_step,
                                      time=time)
        self._write_maps(time_step=time_step)

    def update(self):
        hare = self._animals['hare']
        puma = self._animals['puma']

        hare_map = self._population_maps['hare']
        puma_map = self._population_maps['puma']

        # Create copies of initial maps and arrays for PPM file maps.
        # Reuse these so we don't need to create new arrays going
        # round the simulation loop.
        new_hare_map = hare_map.copy()
        new_puma_map = puma_map.copy()

        for row in range(1, self._height + 1):
            for col in range(1, self._width + 1):
                if self._landscape[row, col]:

                    new_hare_map[row, col] = \
                        hare_map[row, col] + self._delta_t * (
                            (hare.birth_rate * hare_map[row, col]) -
                            (hare.death_rate * hare_map[row, col] *
                                puma_map[row, col]) + hare.diffusion_rate *
                            ((hare_map[row - 1, col] + hare_map[
                                row + 1, col] + hare_map[row, col - 1] +
                                hare_map[row, col + 1]) -
                                (self._land_neighbours[row, col] *
                                hare_map[row, col])))
                    if new_hare_map[row, col] < 0:
                        new_hare_map[row, col] = 0

                    new_puma_map[row, col] = \
                        puma_map[row, col] + self._delta_t * (
                            (puma.birth_rate * hare_map[row, col] *
                                puma_map[row, col]) -
                            (puma.death_rate * puma_map[row, col]) +
                            puma.diffusion_rate *
                            ((puma_map[row - 1, col] + puma_map[
                                row + 1, col] + puma_map[row, col - 1] +
                                puma_map[row, col + 1]) -
                                (self._land_neighbours[row, col] *
                                puma_map[row, col])))
                    if new_puma_map[row, col] < 0:
                        new_puma_map[row, col] = 0

        # Swap arrays for next iteration.
        hare_map, new_hare_map = utils.swap_objects(hare_map, new_hare_map)
        puma_map, new_puma_map = utils.swap_objects(puma_map, new_puma_map)

        self._population_maps['hare'] = hare_map
        self._population_maps['puma'] = puma_map

    def run(self):
        for i in range(0, self._total_time_steps):
            if not i % self._logging_interval:
                self.log(time_step=i, time=i * self._delta_t)

            self.update()

    @property
    def animals(self):
        return self._animals

    @property
    def landscape(self):
        return self._landscape

    @property
    def population_maps(self):
        return self._population_maps
