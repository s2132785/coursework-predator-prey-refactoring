from predator_prey.animal import Animal
from predator_prey.simulation import SimulationBase, Simulation

__version__ = '1.0'


def get_version():
    return __version__


__all__ = ['Animal', 'SimulationBase', 'Simulation', 'get_version']
